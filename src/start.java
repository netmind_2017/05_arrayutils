import org.apache.commons.lang3.ArrayUtils;

public class start {

	public static void main(String[] args) {
		 
		 String[] fruits = { "Orange", "Apple", "Blueberry", "Guava", "Apple", "Peach", "Orange", "Strawberry" };
		 
		 boolean contains = ArrayUtils.contains(fruits, "Guava");
		 System.out.println("Contains Guava? Answer = " + contains);

		 int indexOfBlueberry = ArrayUtils.indexOf(fruits, "Blueberry");
		 System.out.println("index of Blueberry = " + indexOfBlueberry);

		 int lastIndexOfOrange = ArrayUtils.lastIndexOf(fruits, "Orange");
		 System.out.println("last index of Orange = " + lastIndexOfOrange);
		 
		 //reverse
		 ArrayUtils.reverse(fruits);
		 printArray(fruits);

		
	}
	
	private static void printArray(String array[]) {
	      System.out.println(array.length);
	      
	      for (int i = 0; i < array.length; i++) {
	         if(i != 0) {
	            System.out.print(", ");
	         }
	         System.out.print(array[i]);                     
	      }
	      System.out.println();
	   }
	
}
